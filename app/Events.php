<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $fillable = ['name', 'event_from', 'event_to', 'exclude_dates'];
}
