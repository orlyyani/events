<?php

namespace App\Http\Controllers;

use App\Events;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function index()
    {
        return Events::all();
    }

    public function show ($id){
        return Events::find($id);
    }

    public function store(Request $request)
    {
        return Events::create($request->all());
    }
}
